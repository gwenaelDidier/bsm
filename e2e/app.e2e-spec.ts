import { MyApplyPage } from './app.po';

describe('my-apply App', function() {
  let page: MyApplyPage;

  beforeEach(() => {
    page = new MyApplyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
