import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterialModule} from '@angular/material';
import {RouterModule, Routes} from '@angular/router';
import {AngularFireModule, AuthMethods, AuthProviders} from 'angularfire2';
import {LocalStorageModule} from 'angular-2-local-storage';
import {CalendarModule} from 'angular-calendar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {LOCALE_ID} from '@angular/core';
import {subDays, addDays, addWeeks, subWeeks, addMonths, subMonths} from 'date-fns';
//import { Ng2SelectModule } from 'ng2-material-select';

import {AppComponent} from './app.component';
import {MyHomeComponent} from './my-home/my-home.component';
import {MyToolbarComponent} from './my-toolbar/my-toolbar.component';
import {MyAdminComponent} from './my-admin/my-admin.component';
import {MySettingsFilterPipe} from './pipes/my-settings-filter.pipe';
import {MyCalendarComponent} from './my-calendar/my-calendar.component';

import {MyFirebaseServiceService} from './services';
import { MyCompteComponent } from './my-compte/my-compte.component';
import { MySuiviComponent } from './my-suivi/my-suivi.component';

const appRoutes: Routes = [
  {path: '', component: MyHomeComponent},
  {path: 'acceuil', component: MyHomeComponent},
  {path: 'admin', component: MyAdminComponent},
  {path: 'moncompte', component: MyCompteComponent},
  {path: 'suivi', component: MySuiviComponent},
];

export const firebaseConfig = {
  apiKey: "AIzaSyDpX-JwA84mQ4WW_nmqOohALVfa0SeouZQ",
  authDomain: "mypersonnalpage-156508.firebaseapp.com",
  databaseURL: "https://mypersonnalpage-156508.firebaseio.com",
  storageBucket: "mypersonnalpage-156508.appspot.com",
  messagingSenderId: "292855370774"
};

@NgModule({
  declarations: [
    AppComponent,
    MyHomeComponent,
    MyToolbarComponent,
    MyAdminComponent,
    MySettingsFilterPipe,
    MyCalendarComponent,
    MyCompteComponent,
    MySuiviComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig, {
      provider: AuthProviders.Google,
      method: AuthMethods.Popup
    }),
    LocalStorageModule.withConfig({
      prefix: 'myPage',
      storageType: 'localStorage'
    }),
    FlexLayoutModule.forRoot(),
    CalendarModule.forRoot(),

  ],
  providers: [{provide: LOCALE_ID, useValue: "fr-FR"}, MyFirebaseServiceService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
