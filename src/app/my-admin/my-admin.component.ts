import {Component, OnInit} from '@angular/core';
import {AngularFire, AuthProviders, FirebaseListObservable} from 'angularfire2';
import {LocalStorageService} from 'angular-2-local-storage';
import {MyFirebaseServiceService} from '../services';

@Component({
  selector: 'app-my-admin',
  templateUrl: './my-admin.component.html',
  styleUrls: ['./my-admin.component.scss']
})
export class MyAdminComponent implements OnInit {

  public user = {};
  public settings: FirebaseListObservable<any[]>;
  public userExist: Boolean;
  public preferences: String = 'preferences';
  public applications: String = 'applications';
  public filesToUpload:FileList;
  public resultToUpload:FileList;
  public mesPhotos:Array<File>;
  public mesEvenements:Array<any>;

  private checkUserInfoExist(user): any {
    const userUid = this.af.database.list('/' + user.uid);
    let isExist: Boolean;

    return userUid.map(
      (uid) => {
        isExist = (uid.length > 0 ? true : false);
        return isExist;
      }
    );
  }

  private setSettingsByUser(user) {
    this.settings = this.af.database.list('/' + user.uid);

    /*this.settings.subscribe(
     setting => {
     for(let s in setting){
     if(setting[s].$key === 'preferences'){
     this.preferences = setting[s];
     }
     }

     }
     );*/

  }

  constructor(public af: AngularFire, public localStorageService: LocalStorageService, private _fs: MyFirebaseServiceService) {
    this.af.auth.subscribe(user => {
      if (user) {
        // user logged in
        this.user = user;
        localStorageService.set('user', this.user);

        this.checkUserInfoExist(user).subscribe(
          isExist => {
            if (isExist) {
              console.log("chargement des parametres");
              this.setSettingsByUser(user);
            } else {
              const uid = user.uid;

              const setting = this.af.database.list('/');
              const userinfo = {
                "compte": user.google,
                "infos": {
                  "club": "_",
                  "licence": "_",
                  "sexe": "M"
                }
              };

              setting.update(uid, userinfo);
              setTimeout(() => {
                this.setSettingsByUser(user);
              }, 1000);
            }
          }
        );

      }
      else {
        // user not logged in
        this.user = {};
      }
    });


    this.localStorageService = localStorageService;

    if (localStorageService.get('user') != null) {
      this.user = localStorageService.get('user');
    }
  }

  loginGoogle() {
    this.af.auth.login({
      provider: AuthProviders.Google
    });
  }

  logout() {
    this.af.auth.logout();
    this.localStorageService.remove('user');
  }

  getListEvents():Array<any> {
    this._fs.getEvents().subscribe(
      evenements => {
        this.mesEvenements = evenements;
      }
    );
    return [];
  }

  onChange(event) {
    this.filesToUpload = event.target.files;
    console.log( this.filesToUpload);
  }
  onChangeResults(event) {
    this.resultToUpload = event.target.files;
    console.log( this.resultToUpload);
  }

  runUpload(id):void{

    for (let i = 0, length = this.filesToUpload.length; i < length; i++) {
      this._fs.uploadPhoto(id, this.filesToUpload.item(i))
    }

  }

  ngOnInit() {
    this.getListEvents();
  }

}
