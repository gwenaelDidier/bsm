import {Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import {CalendarEvent} from 'angular-calendar';
import {subDays, addDays, addHours, addWeeks, subWeeks, addMonths, subMonths, format} from 'date-fns';
import {FirebaseListObservable} from 'angularfire2';
import {Observable} from 'rxjs/Observable';
import {MyFirebaseServiceService} from '../services';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-my-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './my-calendar.component.html',
  styleUrls: ['./my-calendar.component.scss']
})

export class MyCalendarComponent implements OnInit {

  /* declaration des attributs de la classe */
  public viewDate: Date = new Date();
  public view: string = 'month';
  public listEvenements: FirebaseListObservable<any[]>;
  public listInscris: FirebaseListObservable<any[]>;
  public nbInscris: Number = 0;
  public selectedEvent: Object;
  public action: String = '';
  public areaInscriptionEventName: Boolean = false;
  public mesPhotos: Array<any>;

  public inscriptionName: String;

  events: Observable<CalendarEvent[]>;

  /* méthodes publiques */
  public increment(): void {
    this.viewDate = addMonths(this.viewDate, 1);
  };

  public decrement(): void {
    this.viewDate = subMonths(this.viewDate, 1);
  };

  public fetchEvents(): void {
    this.listEvenements = this._fs.getEvents();

    this.events = this.listEvenements
      .map(evenements => {
        return evenements.map(evenement => {
          return {
            start: new Date(evenement.date),
            end: new Date(evenement.date),
            title: evenement.title,
            color: colors[evenement.color],
            heure: evenement.heure,
            id: evenement.id
          }
        })
      });
  };

  /**
   * @param action
   * @param event
   */
  public handleEvent(action: string, event: CalendarEvent): void {

    this.selectedEvent = event;
    this.action = action;
    let id = this.selectedEvent['id'];

    this.listInscris = this._fs.getInscrisForEvent(id);

    this.listInscris.subscribe(
      inscris => this.nbInscris = inscris.length
    );

    this._fs.getPhotos(id).subscribe(
      photos => this.mesPhotos = photos
    );
  };

  public sinscrire(): void {

    if (this.inscriptionName !== '') {
      this.listInscris.push(this.inscriptionName);
      this.areaInscriptionEventName = false;
    }

  };

  constructor(private _fs: MyFirebaseServiceService) {

  }

  ngOnInit() {
    this.fetchEvents();
  }
}
