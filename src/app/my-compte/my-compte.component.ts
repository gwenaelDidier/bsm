import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {MyFirebaseServiceService} from '../services';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-my-compte',
  templateUrl: './my-compte.component.html',
  styleUrls: ['./my-compte.component.scss'],
  providers: [MdSnackBar, MdSnackBarConfig]
})
export class MyCompteComponent implements OnInit {

  public user:any = {};
  public userInfoisAvailable:Boolean = false;
  constructor(public _ls: LocalStorageService,
              private _fs: MyFirebaseServiceService,
              private snackBar: MdSnackBar,
              private snackBarConfig: MdSnackBarConfig) { }

  public validerCompte():void{
    let infos_ = {
      "prenom": this.user.infos.prenom,
      "nom": this.user.infos.nom,
      "telephone": this.user.infos.telephone,
      "naissance": this.user.infos.naissance,
      "club": this.user.infos.club,
      "licence": this.user.infos.licence,
      "sexe": this.user.infos.sexe,
    };
    this._fs.setAccountInfos( this.user.uid, infos_)
      .then(
        _ => {
          this.snackBarConfig = {
            duration: 3000
          };
          this.snackBar.open('compte enregistré', '', this.snackBarConfig);
        }
      )
      .catch(
        error => {
          console.log(error);
        }
      );
  }

  ngOnInit() {
    this.user = this._ls.get("user");
    this._fs.getAccountInfos(this.user.uid).subscribe(
      infos => {
        this.user.infos = infos;
        this.userInfoisAvailable = true;
      }
    );

  }

}
