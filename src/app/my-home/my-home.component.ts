import { Component, OnInit  } from '@angular/core';
import { MyFirebaseServiceService } from '../services';

@Component({
  selector: 'app-my-home',
  templateUrl: './my-home.component.html',
  styleUrls: ['./my-home.component.scss']
})
export class MyHomeComponent implements OnInit {

	public myTitle:String = 'Boule Sportive Miremontaise';
  public mesPhotos:Array<any>;

  constructor(private _fs: MyFirebaseServiceService) {

  }



  ngOnInit() {

  }
}
