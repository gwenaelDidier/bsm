/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MySuiviComponent } from './my-suivi.component';

describe('MySuiviComponent', () => {
  let component: MySuiviComponent;
  let fixture: ComponentFixture<MySuiviComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MySuiviComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MySuiviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
