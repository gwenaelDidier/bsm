import { Component, OnInit } from '@angular/core';
import { MyFirebaseServiceService } from '../services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/*interface Player {
  prenom:String,
  nom:String,
  licence:String
}*/

@Component({
  selector: 'app-my-suivi',
  templateUrl: './my-suivi.component.html',
  styleUrls: ['./my-suivi.component.scss']
})


export class MySuiviComponent implements OnInit {

  public mesJoueurs:Array<any>;
  public newPlayer = {
    "prenom": null,
    "nom":null,
    "licence": null
  };
  public resultats:Array<any>;

  public areaToAddResultats:boolean = false;
  public areaToAddPlayer:boolean = false;

  public playerAddResultsObj:Object = {};
  public totalPoints:number = 0;
  public anneeChoisie:number;
  public anneeRange:Array<number> = [2016, 2017];
  public playerSelected:Object = null;

  public listPlayersAvailable:boolean = false;

  newPlayerForm: FormGroup;
  PlayerResultsForm: FormGroup;

  resultsOptions:Array<any> = ['vainqueur', 'finaliste', 'demi', 'quart', 'huitieme', 'seizieme', 'trente-deux'];

  public addPlayer(value: any){
    let prenom = value.prenom;
    let nom = value.nom;
    let licence = value.licence;

    if(prenom !== undefined && nom !== undefined && licence !== undefined){
      this._fs.addNewPlayer(prenom, nom, licence)
        .then(
        _ => {
         this.newPlayer.prenom = null;
          this.newPlayer.nom = null;
          this.newPlayer.licence = null;
        }
      )
        .catch(
          error => {
            console.log(error);
          }
        );
    }
  }

  public getPlayers(){
    this._fs.getAllPlayers().subscribe(
      players => {
        this.mesJoueurs = players;
        this.listPlayersAvailable = true;
      }
    );
  }

  public selectPlayer(joueur){
    this.playerSelected = joueur;
    this.totalPoints = 0;
    this._fs.getResultsOfficielsForPlayer(this.anneeChoisie, joueur).subscribe(
      res => {
        if(res.length > 0){
          this.resultats = res;
          res.map(
            res_ => {
              this.totalPoints+= parseInt(res_.points);
            }
          )
        }
      }
    );

  }

  public showAreaAddAplayer(){
    this.areaToAddPlayer = true;
    this.areaToAddResultats = false;
  }

  public showAreaResultsForPlayer(joueur){
    this.areaToAddPlayer = false;
    this.areaToAddResultats = true;

      this.PlayerResultsForm.controls['prenom'].setValue(joueur.prenom);
      this.PlayerResultsForm.controls['nom'].setValue(joueur.nom);
      this.PlayerResultsForm.controls['licence'].setValue(joueur.licence);
  }

  public addResultsForPlayer(resultsForm){

    let licence_ = resultsForm.controls['licence'].value;
    let results_ = {

      'annee': resultsForm.controls['annee'].value,
      'date': resultsForm.controls['date'].value,
      'lieu': resultsForm.controls['lieu'].value,
      'stade': resultsForm.controls['stade'].value,
      'points': resultsForm.controls['points'].value,
      'nbEquipes': resultsForm.controls['nbEquipes'].value,
      'formation': resultsForm.controls['formation'].value
    };

    this._fs.addResultsForPlayer(licence_, results_, 'officiels');

    setTimeout(() => {
      resultsForm.reset();
      licence_ = null;
      results_ = null;
      this.areaToAddResultats = false;
    }, 1000);
  }

  constructor(private _fs: MyFirebaseServiceService, fb: FormBuilder) {
    this.newPlayerForm = fb.group({
      'prenom': '',
      'nom': '',
      'licence': ''
    });

    this.PlayerResultsForm = fb.group({
      'prenom': '',
      'nom': '',
      'licence': '',
      'annee': '',
      'date': '',
      'lieu': '',
      'formation': '',
      'nbEquipes': 0,
      'stade': '',
      'points': 0
    });
  }

  ngOnInit() {
    this.getPlayers();
    this.anneeChoisie = new Date().getFullYear();
  }

}
