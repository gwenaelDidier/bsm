import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mySettingsFilter'
})
export class MySettingsFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    console.log(value);
    console.log(args);

    if(value !== null){
    	return value.filter(
    		val => val.$key.toLowerCase() === args.toLowerCase()
    	);
    }else{
    	return [];
    }
    
  }

}
