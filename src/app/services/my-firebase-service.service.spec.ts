/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MyFirebaseServiceService } from './my-firebase-service.service';

describe('MyFirebaseServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyFirebaseServiceService]
    });
  });

  it('should ...', inject([MyFirebaseServiceService], (service: MyFirebaseServiceService) => {
    expect(service).toBeTruthy();
  }));
});
