import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

import * as firebase from 'firebase';

@Injectable()
export class MyFirebaseServiceService {

  storageRef:any;
  photosRepRef:any;
  photoRef:any;
  idEvent:Number;

  constructor(public af: AngularFire) {

    this.storageRef = firebase.storage().ref();
    this.photosRepRef = this.storageRef.child('/photos/');
  }

  /**
   *
   * @param id
   * @param file
   */
  uploadPhoto(id, file){

    this.idEvent = id;
    this.photoRef = this.photosRepRef.child(file.name);
    let uploadTask = this.photoRef.put(file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => file.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
      (error) => {},
      () => {
        file.url = uploadTask.snapshot.downloadURL;
        file.isUploading = false;
        this.saveImage({ name: file.name, url: file.url });
      }
    );
  }

  getPhotos(id): FirebaseListObservable<any[]>{
    return this.af.database.list('/evenements/' + id + '/photos');
  }

  getEvents(): FirebaseListObservable<any[]>{
    return this.af.database.list('evenements');
  }

  getInscrisForEvent(id): FirebaseListObservable<any[]>{
    return this.af.database.list('/evenements/' + id + '/inscriptions');
  }

  getAccountInfos(uid): FirebaseObjectObservable<any[]>{
    return this.af.database.object(uid + '/infos');
  }

  setAccountInfos(uid, infos):firebase.Promise<void>{
      return this.af.database.list(uid).update('infos', infos);
  }

  addNewPlayer(prenom:String, nom:String, licence:String):firebase.Promise<void>{
    let newPlayer = {
      "prenom": prenom,
      "nom": nom,
      "licence": licence
    };
    return this.af.database.list('/suivi/').push(newPlayer);
  }

  getAllPlayers():FirebaseListObservable<any[]>{
    return this.af.database.list('/suivi/');
  }

  addResultsForPlayer(licence, player, categorie){
    const suivi_ = this.af.database.list('/suivi/', {
      query: {
        orderByChild: 'licence',
        equalTo: licence,
      }
    });

    suivi_.subscribe(
      joueur => {

        this.af.database.list('/suivi/' + joueur[0].$key + '/resultats/' + categorie).push(player).then(
          _ => {
            location.reload();
          }
        )
          .catch(
            error => {
              console.log(error);
            }
          );
      }
    );

  }

  getResultsOfficielsForPlayer(annee, joueur):FirebaseListObservable<any[]>{
    return this.af.database.list('/suivi/' + joueur.$key + '/resultats/officiels', {
      query: {
        orderByChild: 'annee',
        equalTo: annee,
      }
    });
  }

  private saveImage(image:any) {
    this.af.database.list('/evenements/' + this.idEvent + '/photos').push(image);
  }
}
