/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MyGoogleServiceService } from './my-google-service.service';

describe('MyGoogleServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyGoogleServiceService]
    });
  });

  it('should ...', inject([MyGoogleServiceService], (service: MyGoogleServiceService) => {
    expect(service).toBeTruthy();
  }));
});
