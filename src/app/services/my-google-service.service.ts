import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class MyGoogleServiceService {

  constructor(private _http: Http) { }

  getPhotosFromDrive(){
    return this._http.get('https://www.googleapis.com/drive/v3/files').map(res => res.json());
  }

}
